document.getElementById('btn-mine').addEventListener('click', function () {
  document.getElementById('modal').style.display = 'grid';
  document.getElementById('backdrop').style.display = 'grid';
  window.scrollTo(0, 0, 'smooth');
  document.body.style.overflow = 'hidden';
});

document.getElementById('btn-mine-2').addEventListener('click', function () {
  document.getElementById('modal').style.display = 'grid';
  document.getElementById('backdrop').style.display = 'grid';
  window.scrollTo(0, 0, 'smooth');
  document.body.style.overflow = 'hidden';
});

document.getElementById('btn-mine-3').addEventListener('click', function () {
  document.getElementById('modal').style.display = 'grid';
  document.getElementById('backdrop').style.display = 'grid';
  window.scrollTo(0, 0, 'smooth');
  document.body.style.overflow = 'hidden';
});

document.getElementById('backdrop').addEventListener('click', function () {
  document.getElementById('modal').style.display = 'none';
  document.getElementById('backdrop').style.display = 'none';
  document.body.style.overflow = 'auto';
});

document.getElementById('users').oninput = function () {
  this.nextElementSibling.value = this.value;
  if (this.value > 0 && this.value <= 10) {
    document.getElementById('plan').innerHTML = 'Free';
  } else if (this.value > 10 && this.value <= 20) {
    document.getElementById('plan').innerHTML = 'Pro';
  } else if (this.value > 20) {
    document.getElementById('plan').innerHTML = 'Enterprise';
  }
};
